package main

import (
	"fmt"
	"regexp"
	"strings"

	"gitlab.oit.duke.edu/dul-go/journal"
)

// SendSchemaToSolr sends data from schema struct
// to solr instance as defined by settings
func SendSchemaToSolr(schema Schema, settings Settings) {
	reHTTP400 := regexp.MustCompile(`^400`)

	url := fmt.Sprintf("%s/%s/schema", settings.Solr.Host, settings.Solr.Core)
	for _, t := range schema.Types.FieldTypes {
		var sb strings.Builder
		sb.WriteString(fmt.Sprintf("{\"add-field-type\":%s}\n", t.JSONString()))

		// request for 'add-field-type'
		resp, err := SolrAPIRequest([]byte(sb.String()), url)
		if err != nil {
			if reHTTP400.Match([]byte(err.Error())) {
				journal.Warning.Printf("[SendSchemaToSolr] field already exists. attempting to replace...")
				// field type already exists. Replace the field type
				sb.Reset()
				sb.WriteString(fmt.Sprintf("{\"replace-field-type\":%s}\n", t.JSONString()))
				resp, err = SolrAPIRequest([]byte(sb.String()), url)
			} else {
				journal.Error.Printf("%s\n", err)
			}
		}
		journal.Debug.Println(resp)
	}

	// TODO send the fields

	// TODO send the copy fields
}
