package main

import (
	"fmt"
	"strings"
)

//Settings for the application
type Settings struct {
	Solr struct {
		Host string `yaml:"host" envconfig:"SP_SOLR_HOST"`
		Core string `yaml:"core" envconfig:"SP_SOLR_CORE"`
	}

	Debug bool

	// This file holds the non-environment variable settings
	ConfigFileName string

	// Name of the schema file.
	// 9.5 times out of 10, this will be "schema.xml"
	// But we'll add flag or config setting for override
	SchemaFileName string `envconfig:"SP_SCHEMA_XML_PATH"`
}

func (s Settings) String() string {
	var sb strings.Builder
	sb.WriteString(fmt.Sprintf("* configuration file = [%s]\n", s.ConfigFileName))
	sb.WriteString(fmt.Sprintf("* schema file name = [%s]\n", s.SchemaFileName))
	sb.WriteString(fmt.Sprintf("* solr host = [%s]\n", s.Solr.Host))
	sb.WriteString(fmt.Sprintf("* solr core = [%s]\n", s.Solr.Core))

	return sb.String()
}
