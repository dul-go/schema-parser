module gitlab.oit.duke.edu/dul-go/schema-parser

go 1.15

require (
	github.com/kelseyhightower/envconfig v1.4.0
	gitlab.oit.duke.edu/dul-go/journal v0.0.0-20190219194120-2a5b5450b878
	gopkg.in/yaml.v2 v2.4.0
)
