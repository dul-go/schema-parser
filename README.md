# Schema Parser

## Name
Schema-to-API Parser

## Description
Import the contents of a local `schema.xml` file into a Solr core's managed schema using the Solr API.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Installation
Install `golang`, following [these instructions](https://golang.org/doc/install)

Before you attempt to run this...
### Downloading Go Modules from Gitlab

**This step is very important!!!** 👇  
    
This project uses packages hosted on OIT's GitLab server. The steps below will ensure 
you're able to download these dependencies successfully.

#### **Create GitLab Personal Access Token**
Create a Personal Access Token with the following permissions:
- read_repository
- read_api
  
*Hang on to this token...*  

#### **Create `.netrc` File**
Create a `.netrc` file at the root of your home directory (or somewhere on 
your target VM when running in `production` mode). 
  
Then, add the following:
```sh
machine gitlab@oit.duke.edu
login git@gitlab.oit.duke.edu
password <your-newly-created-token>
```
  
**Now, you're ready to play...**

## Usage

### Using Docker
```sh
$ git checkout https://gitlab.oit.duke.edu/dul-go/schema-parser.git
$ docker build --tag schemaparser -f .docker/Dockerfile .

# Here, you can use the schema.xml that's in the checked-out repo, 
# or supply your own.

# You can set the Solr host and core either in config.yml or
# using environment variabes
$ docker run --rm -v ${PWD}/schema.xml:/dist/schema.xml \
  -e SP_SOLR_HOST=localhost:8983 -e SP_SOLR_CORE=gettingStarted \
  schemaparser
```
### Non-Docker
```sh
$ git clone https://gitlab.oit.duke.edu/dul-go/schema-parser.git
# or if you intend to contribute code:
# git clone git@gitlab.oit.duke.edu:dul-go/schema-parser.git

$ cd /path/to/schema-parser

# update dependencies
$ go mod tidy

$ go run .
```
## Support
Send a note to `derrek.croney@duke.edu` or open in issue on this project.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
