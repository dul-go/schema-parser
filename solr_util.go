package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
)

// SolrAPIRequest attempts to add field-type via
// Solr's API
func SolrAPIRequest(jsonData []byte, url string) (string, error) {
	reHTTP200 := regexp.MustCompile(`^200`)

	request, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonData))
	request.Header.Set("Content-Type", "application/json; charset=UTF-8")

	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		return "", err
	}

	// Read the response's body regardless of the
	// HTTP status
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return "", err
	}

	// return the response body (error message) and status code
	if !reHTTP200.Match([]byte(response.Status)) {
		return string(body), fmt.Errorf("%s", response.Status)
	}

	return string(body), nil
}
