package main

import (
	"encoding/xml"
	"fmt"
	"strings"
)

//Schema represents <schema> tag
type Schema struct {
	XMLName    xml.Name    `xml:"schema"`
	Types      Types       `xml:"types"`
	Fields     Fields      `xml:"fields"`
	UniqueKey  string      `xml:"uniqueKey"`
	CopyFields []CopyField `xml:"copyField"`
	Name       string      `xml:"name,attr"`
	Version    string      `xml:"version,attr"`
}

// Pretty-print (kind of) our struct when asked.
func (s Schema) String() string {
	str := fmt.Sprintf("\nSchema: name: %s; version: %s\n\nTypes:\n%v\n\nFields:\n%v\n\nCopyFields:\n%v\n",
		s.Name, s.Version, s.Types, s.Fields, s.CopyFields)
	return str
}

//Types contains collection of FieldTypes
type Types struct {
	XMLName    xml.Name    `xml:"types"`
	FieldTypes []FieldType `xml:"fieldType"`
}

func (t Types) String() string {
	return fmt.Sprintf("FieldTypes: \n%v", t.FieldTypes)
}

//Fields holds a collection of <field/> elements
type Fields struct {
	XMLName xml.Name `xml:"fields"`
	Fields  []Field  `xml:"field"`
}

func (f Fields) String() string {
	return fmt.Sprintf("Fields: \n%v", f.Fields)
}

//FieldType holds types of fields in our schema
type FieldType struct {
	XMLName                   xml.Name   `xml:"fieldType"`
	Analyzers                 []Analyzer `xml:"analyzer"`
	Class                     string     `xml:"class,attr"`
	Name                      string     `xml:"name,attr"`
	PositionIncrementGap      string     `xml:"positionIncrementGap,attr"`
	AutoGeneratePhraseQueries string     `xml:"autoGeneratePhraseQueries,attr"`
	GenerateNumberParts       string     `xml:"generateNumberParts,attr"`
	SortMissingLast           string     `xml:"sortMissingLast,attr"`
	OmitNorms                 string     `xml:"omitNorms"`
	PrecisionStep             string     `xml:"precisionStep"`
	Stored                    string     `xml:"stored,attr"`
	Indexed                   string     `xml:"indexed,attr"`
	MultiValued               string     `xml:"multiValued,attr"`
	DocValues                 string     `xml:"docValues,attr"`
	SubFieldSuffix            string     `xml:"subFieldSuffix"`
}

func (t FieldType) String() string {
	var sb strings.Builder
	sb.WriteString(fmt.Sprintf("class: %s | name: %s | positionIncrementGap [%s] | stored [%s]\n",
		t.Class, t.Name, t.PositionIncrementGap, t.Stored))
	sb.WriteString(fmt.Sprintf("Analyzers:\n%v\n", t.Analyzers))
	return sb.String()
}

//JSONString returns non-pretty JSON string
func (t FieldType) JSONString() string {
	var sb strings.Builder
	sb.WriteString("{")
	sb.WriteString(fmt.Sprintf("\"name\":\"%s\"", t.Name))
	if t.Class != "" {
		sb.WriteString(fmt.Sprintf(",\"class\":\"%s\"", t.Class))
	}
	if t.PositionIncrementGap != "" {
		sb.WriteString(fmt.Sprintf(",\"positionIncrementGap\":\"%s\"", t.PositionIncrementGap))
	}
	if t.AutoGeneratePhraseQueries != "" {
		sb.WriteString(fmt.Sprintf(",\"autoGeneratePhraseQueries\":\"%s\"", t.AutoGeneratePhraseQueries))
	}
	if t.GenerateNumberParts != "" {
		sb.WriteString(fmt.Sprintf(",\"generateNumberParts\":\"%s\"", t.GenerateNumberParts))
	}
	if t.SortMissingLast != "" {
		sb.WriteString(fmt.Sprintf(",\"sortMissingLast\":\"%s\"", t.SortMissingLast))
	}
	if t.OmitNorms != "" {
		sb.WriteString(fmt.Sprintf(",\"omitNorms\":\"%s\"", t.OmitNorms))
	}
	if t.PrecisionStep != "" {
		sb.WriteString(fmt.Sprintf(",\"precisionStep\":\"%s\"", t.PrecisionStep))
	}
	if t.Stored != "" {
		sb.WriteString(fmt.Sprintf(",\"stored\":\"%s\"", t.Stored))
	}
	if t.Indexed != "" {
		sb.WriteString(fmt.Sprintf(",\"indexed\":\"%s\"", t.Indexed))
	}
	if t.MultiValued != "" {
		sb.WriteString(fmt.Sprintf(",\"multiValued\":\"%s\"", t.MultiValued))
	}
	if t.DocValues != "" {
		sb.WriteString(fmt.Sprintf(",\"docValues\":\"%s\"", t.DocValues))
	}
	if t.SubFieldSuffix != "" {
		sb.WriteString(fmt.Sprintf(",\"subFieldSuffix\":\"%s\"", t.SubFieldSuffix))
	}
	for _, a := range t.Analyzers {
		if a.Type == "index" {
			sb.WriteString(fmt.Sprintf(",\"indexAnalyzer\":%s", a.JSONString()))
		} else if a.Type == "query" {
			sb.WriteString(fmt.Sprintf(",\"queryAnalyzer\":%s", a.JSONString()))
		}
	}
	sb.WriteString("}")
	return sb.String()
}

//Analyzer contains Tokenizers and Filters
type Analyzer struct {
	XMLName     xml.Name     `xml:"analyzer"`
	Type        string       `xml:"type,attr"`
	Tokenizer   Tokenizer    `xml:"tokenizer"`
	Filters     []Filter     `xml:"filter"`
	CharFilters []CharFilter `xml:"charFilter"`
}

func (a Analyzer) String() string {
	return fmt.Sprintf("type: %s\nTokenizers:\n%v\n\nFilters:\n%v\n\n",
		a.Type, a.Tokenizer, a.Filters,
	)
}

//JSONString returns non-pretty JSON string
func (a Analyzer) JSONString() string {
	var sb strings.Builder

	sb.WriteString("{")
	sb.WriteString(fmt.Sprintf("\"tokenizer\":%s,\"filters\":[", a.Tokenizer.JSONString()))
	for _, f := range a.Filters {
		sb.WriteString(f.JSONString())
	}
	sb.WriteString("]")
	sb.WriteString("}")

	return sb.String()
}

//Tokenizer type
type Tokenizer struct {
	XMLName   xml.Name `xml:"tokenizer"`
	Class     string   `xml:"class,attr"`
	Delimiter string   `xml:"delimiter,attr"`
}

func (t Tokenizer) String() string {
	return fmt.Sprintf("class: %s\n", t.Class)
}

//JSONString returns non-pretty JSON string
func (t Tokenizer) JSONString() string {
	var sb strings.Builder
	sb.WriteString("{")
	sb.WriteString(fmt.Sprintf("\"class\":\"%s\"", t.Class))
	if t.Delimiter != "" {
		sb.WriteString(fmt.Sprintf(",\"delimiter\":\"%s\"", t.Delimiter))
	}
	sb.WriteString("}")

	return sb.String()
}

//CharFilter - character filter
type CharFilter struct {
	XMLName     xml.Name `xml:"charFilter"`
	Class       string   `xml:"class"`
	Replacement string   `xml:"replacement"`
	Pattern     string   `xml:"pattern"`
}

//Filter represents an Analyzer filter
type Filter struct {
	XMLName          xml.Name `xml:"filter"`
	Class            string   `xml:"class,attr"`
	IgnoreCase       string   `xml:"ignoreCase,attr"`
	Synonyms         string   `xml:"synonyms,attr"`
	Words            string   `xml:"words,attr"`
	Protected        string   `xml:"protected,attr"`
	Encoder          string   `xml:"encoder,attr"`
	PreserveOriginal string   `xml:"preserveOriginal"`
}

func (f Filter) String() string {
	var sb strings.Builder
	sb.WriteString(fmt.Sprintf("{Filter >> class [%s] | ignoreCase [%s] | synonyms [%s] | words [%s], protected [%s], encoder [%s]}\n",
		f.Class, f.IgnoreCase, f.Synonyms, f.Words, f.Protected, f.Encoder))
	return sb.String()
}

//JSONString returns non-pretty JSON string
func (f Filter) JSONString() string {
	var sb strings.Builder
	sb.WriteString("{")
	if f.Class != "" {
		sb.WriteString(fmt.Sprintf("\"class\":\"%s\"", f.Class))
	}
	if f.IgnoreCase != "" {
		sb.WriteString(fmt.Sprintf(",\"ignoreCase\":\"%s\"", f.IgnoreCase))
	}
	if f.Synonyms != "" {
		sb.WriteString(fmt.Sprintf(",\"synonyms\":\"%s\"", f.Synonyms))
	}
	if f.Words != "" {
		sb.WriteString(fmt.Sprintf(",\"words\":\"%s\"", f.Words))
	}
	if f.Protected != "" {
		sb.WriteString(fmt.Sprintf(",\"protected\":\"%s\"", f.Protected))
	}
	if f.Encoder != "" {
		sb.WriteString(fmt.Sprintf(",\"encoder\":\"%s\"", f.Encoder))
	}
	if f.PreserveOriginal != "" {
		sb.WriteString(fmt.Sprintf(",\"preserveOriginal\":\"%s\"", f.PreserveOriginal))
	}
	sb.WriteString("}")

	return sb.String()
}

//Field types of fields in our schema
type Field struct {
	XMLName     xml.Name `xml:"field"`
	Name        string   `xml:"name,attr"`
	Stored      string   `xml:"stored,attr"`
	Indexed     string   `xml:"indexed,attr"`
	MultiValued string   `xml:"multiValued,attr"`
}

// CopyField represents <copyField /> element
type CopyField struct {
	XMLName  xml.Name `xml:"copyField"`
	Source   string   `xml:"source,attr"`
	Dest     string   `xml:"dest,attr"`
	MaxChars string   `xml:"maxChars,attr"`
}

// String return basic string representation of a
// copyField
func (f CopyField) String() string {
	return fmt.Sprintf("CopyField >> source [%s] | dest [%s]\n", f.Source, f.Dest)
}

// JSONString - Return JSON-formatted representation
func (f CopyField) JSONString() string {
	var sb strings.Builder
	sb.WriteString(fmt.Sprintf("{\"source\":\"%s\",\"dest\":\"%s\"", f.Source, f.Dest))
	if f.MaxChars != "" {
		sb.WriteString(fmt.Sprintf(",\"maxChars\":%s", f.MaxChars))
	}
	sb.WriteString("}")
	return sb.String()
}
