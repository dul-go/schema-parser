package main

import (
	"encoding/xml"
	"flag"
	"fmt"
	"io/ioutil"
	"os"

	"github.com/kelseyhightower/envconfig"
	"gitlab.oit.duke.edu/dul-go/journal"
	"gopkg.in/yaml.v2"
)

func readSettings(settings *Settings) {
	flag.StringVar(&settings.ConfigFileName, "cfg", "config.yml", "Location of configuration file")
	flag.StringVar(&settings.SchemaFileName, "schema", "schema.xml", "Path to schema file to parse")

	// read the config file
	f, err := os.Open(settings.ConfigFileName)
	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}
	defer f.Close()

	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(settings)
	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}

	// process any available environment variables
	// recognized by the settings
	err = envconfig.Process("", settings)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("Settings:\n%v\n", settings)
}

func main() {
	var settings Settings

	// read settings from
	// - command line args
	// - config file (default = config.yml)
	// - environnment variables
	readSettings(&settings)

	// open the schema.xml file in question
	xmlFile, err := os.Open(settings.SchemaFileName)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	// initialize logging using our
	// own custom-build journal
	// NOTE: use "ioutil.Discard" for a given log level
	// if you wish to ignore it.
	// -
	// To do logging (for instance, debug):
	// journal.Debug.Printf(...) or journal.Debug.Println(...)
	journal.InitLogging(
		os.Stdout, // journal.Debug
		os.Stdout, // journal.Info
		os.Stdout, // journal.Warning
		os.Stderr, // journal.Error
	)

	journal.Info.Printf("[main] Successfully opened %s\n", settings.SchemaFileName)
	defer xmlFile.Close()

	byteValue, _ := ioutil.ReadAll(xmlFile)

	// parse the XML file into our "Schema" data struct
	var schema Schema
	xml.Unmarshal(byteValue, &schema)

	for _, f := range schema.Types.FieldTypes {
		journal.Debug.Printf("[main] %s\n", f.JSONString())
	}

	for _, f := range schema.CopyFields {
		journal.Debug.Printf("[main] %s\n", f.JSONString())
	}

	SendSchemaToSolr(schema, settings)

	// fmt.Printf("%v\n", schema)
}
